#!/bin/bash

URL_BIOBAKERY_REPO="http://huttenhower.sph.harvard.edu/biobakery-shop/deb-packages"

function die() {
    echo "$1" >&2
    exit 1
}

# Check for root privileges
test "$(id -u)" -eq 0 || die "This script must be run as root or with sudo" 
which usearch7 2>&1 > /dev/null || {
    if which usearch 2>&1 > /dev/null; then
	ln -s $(which usearch) /usr/bin/usearch7
    else
	die "Unable to find and execute usearch or usearch7. Is it on your $PATH?"
    fi
}

# Install the huttenhower repo if needed
grep -q 'huttenhower' /etc/apt/sources.list || {
    echo "deb $URL_BIOBAKERY_REPO /" \
	| sudo bash -c "cat - >> /etc/apt/sources.list "
    wget -O- -q "${URL_BIOBAKERY_REPO}/biobakery.asc" \
	| sudo apt-key add -
}
sudo apt-get update

# Install required tools
sudo apt-get install -y python-pip
sudo apt-get install -y qiime-cmd

# Install anadama data build framework
pip install -e 'git+https://bitbucket.org/biobakery/anadama.git@master#egg=anadama-0.0.1'
pip install -e 'git+https://bitbucket.org/biobakery/anadama_workflows.git@master#egg=anadama_workflows-0.0.1'

mkdir mbqc_products

# Download greengenes
wget -O- 'ftp://greengenes.microbio.me/greengenes_release/gg_13_5/gg_13_5_otus.tar.gz' | tar -xzf -

# Make link for absolute file path
here=$(readlink -f "${0}" | sed -e 's#\(.*/\).*#\1#g')
ln -s "${here}" /MBQC_analysis

