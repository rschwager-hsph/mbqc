import os
import re
import sys
import glob
from os.path import basename
from itertools import chain

from anadama.util import new_file

from anadama_workflows.general import extract
from anadama_workflows.sixteen import pick_otus_closed_ref
from anadama_workflows.usearch import usearch_stitch, usearch_filter

from Bio import SeqIO

DOIT_CONFIG = {
    'default_tasks': ['mbqc'],
    'continue': True,
    'pipeline_name': "MBQC_16S"
}

datadir    = '/MBQC_analysis/mbqc_data'
scratchdir = "/MBQC_analysis/mbqc_products"
folders    = os.listdir(datadir)
def paired_reads():
    for folder in folders:
        r1 = glob.glob(os.path.join(datadir, folder, "R1",
                                    "*[0-9][0-9]_R?.fastq.gz"))
        r2 = glob.glob(os.path.join(datadir, folder, "R2", 
                                    "*[0-9][0-9]_R?.fastq.gz"))
        r1, r2 = sorted(r1)[:20], sorted(r2)[:20]
        yield zip(r1, r2)

scratch_file = lambda f: new_file(f, basedir=scratchdir)

def unzipped(fname):
    nozip = os.path.splitext(fname)[0]
    base = basename(nozip)
    return scratch_file(base)


def mangle(input_handle, output_handle):
    def _outseqs():
        i = 0
        label = re.sub(r'.*/([a-z0-9]+)_([a-z0-9]+).*', 
                       r'\1.\2_\1.\2',
                       input_handle.name)
        for seq in SeqIO.parse(input_handle, 'fasta'):
            i += 1
            seq.description = seq.id = "%s.%i"%(label, i)
            if len(seq) > 0:
                yield seq
    SeqIO.write(_outseqs(), output_handle, 'fasta')


def filter_rename(inputs, targets):
    with open(targets[0], 'w') as f_out:
        for fname in inputs:
            with open(fname, 'r') as f_in:
                mangle(f_in, f_out)


def concatenate(inputs, targets):
    with open(targets[0], 'w') as f_out:
        for fname in inputs:
            if not os.path.exists(fname) or not os.stat(fname).st_size > 5:
                continue
            with open(fname, 'r') as f_in:
                for line in f_in:
                    f_out.write(line)
                

def task_mbqc():
    done_fastas = list()
    all_file_pairs = chain(*paired_reads())
    for r1, r2 in all_file_pairs:
        yield extract(r1, unzipped(r1))
        yield extract(r2, unzipped(r2))

        stitched_fname = scratch_file( 
            basename(unzipped(r1)).replace('R1','stitched'))
        yield usearch_stitch([unzipped(r1), unzipped(r2)], 
                             stitched_fname, verbose=True)
        
        filtered_fname = stitched_fname.replace(
            'stitched', 'stitched_filtered'
        ).replace(
            '.fastq', '.fasta' 
        )
        yield usearch_filter(stitched_fname, filtered_fname)

        renamed_fname = filtered_fname.replace("filtered", "filtered_renamed")
        yield { "name"     : "filter_rename:%s" %(filtered_fname),
                "actions"  : [(filter_rename, [[filtered_fname]])],
                "targets"  : [renamed_fname],
                "file_dep" : [filtered_fname] }

        tmp_files = [ stitched_fname, filtered_fname, 
                      unzipped(r1), unzipped(r2) ]
        yield { "name"     : "truncate_tmp_files:"+tmp_files[0],
                "actions"  : [ "echo > '%s'"%(f) for f in tmp_files ],
                "file_dep" : tmp_files }
        
        done_fastas.append(renamed_fname)

    merged_fname = scratch_file("mbqc_all.fasta")
    yield { "name"     : "concatenate_all_sequences",
            "actions"  : [(concatenate, [done_fastas])],
            "file_dep" : done_fastas,
            "targets"  : [merged_fname] }

    final_otu_fname = scratch_file("final_otus")
    yield pick_otus_closed_ref( 
        merged_fname, final_otu_fname,
        verbose=True, qiime_opts={ 
            "taxonomy_fp"   : ("/MBQC_analysis/"
                               "gg_13_5_otus/taxonomy/97_otu_taxonomy.txt"),
            "reference_fp"  : ("/MBQC_analysis/"
                               "gg_13_5_otus/rep_set/97_otus.fasta"),
            "parallel"      : "",
            "jobs_to_start" : "2"
        }
    )
    
