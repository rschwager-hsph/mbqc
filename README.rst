#############
MBQC analysis
#############

Prerequisites
=============

- Ubuntu 14.04
- `Usearch v7 <http://www.drive5.com/usearch/download.html>`_

Installation
============
Use the Installation script ``install.sh`` to install necessary software::

  $ sudo bash install.sh

Copy/move the raw sequence data into the current directory under a
directory named ``mbqc_data``, keeping the same directory
structure. This shouldn't be much more complicated than
``mv -iv /PATH/TO/MBQC/DATA ./mbqc_data``.

Running the Analysis
====================
Kick it off with the following command::

  $ anadama run -f mbqc.py --reporter=verbose

We ran the analysis using 6 CPUs and redirected output to a file named
``mbqc_run.log`` with the following command::

  $ anadama run -f mbqc.py --reporter=verbose 2>&1 | tee mbqc_run.log